import * as UserActionCreators from "../../redux/actions/user/index";

export default {
  ...UserActionCreators,
};
