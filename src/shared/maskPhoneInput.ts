import { ChangeEvent } from "react";

export const handlePhoneFormat = (
  event: ChangeEvent<HTMLInputElement> | any,
  onChange?: (e: string) => void
) => {
  let formattedInputPhone = "";
  if (onChange) {
    let inputNumberValue = event.target.value.replace(/\D/g, "");
    onChange(inputNumberValue);

    if (["7", "8", "9"].indexOf(inputNumberValue[0]) > -1) {
      formattedInputPhone = "+7" + " ";
      if (inputNumberValue.length > 1) {
        formattedInputPhone += "(" + inputNumberValue.substring(1, 4);
      }
      if (inputNumberValue.length >= 5) {
        formattedInputPhone += ") " + inputNumberValue.substring(4, 7);
      }
      if (inputNumberValue.length >= 8) {
        formattedInputPhone += "-" + inputNumberValue.substring(7, 9);
      }
      if (inputNumberValue.length >= 10) {
        formattedInputPhone += "-" + inputNumberValue.substring(9, 11);
      }
      onChange(formattedInputPhone);
      if (event.target.value.length !== event.target.selectionStart) {
        event.target.setSelectionRange(
          event.target.selectionStart,
          event.target.selectionStart
        );

        if (event.nativeEvent.data && /\D/g.test(event.nativeEvent.data)) {
          onChange(formattedInputPhone);
        } else {
          onChange(event.target.value);
        }
      }
    } else {
      onChange("+" + inputNumberValue);
    }
  }
};
export const handlePaste = (e: any, onChange?: (e: string) => void) => {
  let pasted = e.clipboardData;
  let inputNumberValue = e.target.value.replace(/\D/g, "");
  if (pasted) {
    let pastedText = pasted.getData("Text");
    if (/\D/g.test(pastedText)) {
      onChange && onChange(inputNumberValue);
    }
  }
};
