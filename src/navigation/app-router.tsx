import { FC } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import BaseLayout from "../elements/layouts/base-layout/BaseLayout";
import MainPage from "../elements/screens/main/MainPage";
// import UserPage from "../elements/screens/main/UserPage";

const AppRouter: FC = () => {
  return (
    <BaseLayout>
      <BrowserRouter>
        <Routes>
          <Route path={""} element={<MainPage />} />
        </Routes>
      </BrowserRouter>
    </BaseLayout>
  );
};
export default AppRouter;
