import { FC } from "react";
import { ReactComponent as MainIcon } from "../../../assets/image/icon/main.svg";
import "./Style.scss";

const Logo: FC = () => {
  return (
    <div className="logo">
      <MainIcon />
      <h2 className="title">гросс маркет</h2>
    </div>
  );
};
export default Logo;
