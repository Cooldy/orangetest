import Card from "../../components/Card/Card";
import Carousel from "../../components/carousel/Carousel";
import "./Style.scss";
import baker from "../../../assets/image/cards/baker.png";
import cook from "../../../assets/image/cards/cook.png";
import driver from "../../../assets/image/cards/driver.png";
import merchandiser from "../../../assets/image/cards/merchandiser.png";
import receiver from "../../../assets/image/cards/receiver.png";
import salesman from "../../../assets/image/cards/salesman.png";
import { FC } from "react";
import { v4 as uuidv4 } from "uuid";

const CardSlider: FC = () => {
  const sliderItem = [
    {
      id: uuidv4(),
      src: baker,
      title: "пекарь",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: driver,
      title: "кассир",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: cook,
      title: "повар",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: driver,
      title: "водитель",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: merchandiser,
      title: "товаровед",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: receiver,
      title: "приемщик",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
    {
      id: uuidv4(),
      src: salesman,
      title: "продавец",
      text: "Доставка товара по магазинам и гипермаркетам компаниив обслуживаемом регионе",
    },
  ];

  return (
    <section className="section-jobs">
      <h3 className="section-jobs__title">вакансии в гросс маркете</h3>
      <Carousel pageWidth={21} classNameButton="card-button" infinite={true}>
        {sliderItem.map((slide) => {
          return (
            <Card
              key={slide.id}
              titleInfo={slide.title}
              className="card-item"
              src={slide.src}
              text={slide.text}
            />
          );
        })}
      </Carousel>
    </section>
  );
};
export default CardSlider;
