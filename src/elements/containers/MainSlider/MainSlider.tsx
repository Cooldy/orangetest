import { FC } from "react";
import "./Style.scss";
import InfoRadius from "../../components/InfoRadius/InfoRadius";
import slide1 from "../../../assets/image/main-section/mainDesktop.png";
import slide2 from "../../../assets/image/main-section/mainDesktopTwo.png";
import Carousel from "../../components/carousel/Carousel";
import { v4 as uuidv4 } from "uuid";

const MainSlider: FC = () => {
  const slideArray = [
    {
      id: uuidv4(),
      src: slide1,
      text: "У тебя к этому талант",
      name: "валентин",
      job: "пекарь",
      classOne: "slider-block__info-one",
      classTwo: "slider-block__info-two",
    },
    {
      id: uuidv4(),
      src: slide2,
      text: "У тебя всё под контролем",
      name: "ксения",
      job: "товаровед",
      classOne: "slider-block__info-one_two",
      classTwo: "slider-block__info-two_two",
    },
  ];

  return (
    <Carousel pageWidth={100} classNameButton="button-slider">
      {slideArray.map((slide) => {
        return (
          <section key={slide.id} className="section-main">
            <div key={slide.id} className="slider-block">
              <div className="slider-block__text">
                <h1 className="slider-block__text_title">{slide.text}</h1>
              </div>
              <div className="slider-block-image">
                <img
                  loading="eager"
                  className="slider-block__image"
                  src={slide.src}
                  alt="person"
                />
                <InfoRadius className={slide.classOne} title={slide.job} />
                <InfoRadius className={slide.classTwo} title={slide.name} />
              </div>
            </div>
          </section>
        );
      })}
    </Carousel>
  );
};
export default MainSlider;
