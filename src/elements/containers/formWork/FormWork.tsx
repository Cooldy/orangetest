import { FC, useState } from "react";
import Input from "../../components/input/Input";
import Modal from "../../components/modal/Modal";
import Select from "../../components/select/Select";

import "./Style.scss";

interface IFormWork {
  activeModal: boolean;
  setModalActive: (active: boolean) => void;
}

const FormWork: FC<IFormWork> = ({ activeModal, setModalActive }) => {
  const list = [
    { id: 1, title: "товаровед" },
    { id: 2, title: "продавец" },
    { id: 3, title: "кассир" },
    { id: 4, title: "пекарь" },
    { id: 5, title: "водитель" },
    { id: 6, title: "приемщик" },
    { id: 7, title: "пекарь" },
  ];

  const option = [
    { id: 1, title: "мужской", value: "man" },
    { id: 2, title: "женский", value: "wooman" },
  ];
  const [valuePhone, setValuePhone] = useState<string>("");

  const handleChangePhone = (value: string) => {
    setValuePhone(value);
  };

  return (
    <Modal active={activeModal} setActive={setModalActive}>
      <h2 className="form-work__title">Работа твоей мечты</h2>

      <Select label="Вакансия" required={true} option={list} />
      <Input label="ФИО" required={true} />
      <div className="form-flex">
        <Input
          className="form-input"
          label="Дата рождения"
          required={true}
          type="date"
          name="birthday"
        />
        <Input
          className="form-input_radio"
          label="Пол"
          type="radio"
          optionRadio={option}
          name="manOrWooman"
        />
      </div>
      <div className="form-flex">
        <Input
          className="form-input"
          label="Контакный телефон"
          name="phone"
          required={true}
          type="tel"
          valuePhone={valuePhone}
          onChange={(e) => handleChangePhone(e)}
        />

        <Input label="Электронная почты" required={true} type="text" />
      </div>
      <Input textClass="form-input__text" label="Резюме" type="textarea" />
      <button type="submit" className="form-button">
        Отправить
      </button>
    </Modal>
  );
};
export default FormWork;
