import { FC, useState } from "react";
import { ReactComponent as Telephone } from "../../../assets/image/icon/telephone.svg";
import FormWork from "../formWork/FormWork";
import Logo from "../logo/Logo";
import "./Style.scss";

const BaseHeader: FC = () => {
  const [modalActive, setModalActive] = useState<boolean>(false);

  return (
    <header className="header">
      <Logo />
      <Telephone className="header-info__mobile" />
      <div className="header-info">
        <span className="header-info__title">+7 (926) 433-14-16</span>
        <button className="header__buttom" onClick={() => setModalActive(true)}>
          заполнить анкету
        </button>
      </div>
      <FormWork setModalActive={setModalActive} activeModal={modalActive} />
    </header>
  );
};
export default BaseHeader;
