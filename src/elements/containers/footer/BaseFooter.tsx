import { FC } from "react";
import Logo from "../logo/Logo";
import { ReactComponent as Faceboock } from "../../../assets/image/icon/facebook.svg";
import { ReactComponent as VK } from "../../../assets/image/icon/vk.svg";
import "./Style.scss";

const BaseFooter: FC = () => {
  return (
    <footer className="footer">
      <div className="footer-content">
        <div className="block-info">
          <Logo />
          <div className="block-social">
            <h4 className="footer-title">поделиться</h4>
            <div className="social-icons">
              <Faceboock />
              <VK className="footer-icon-vk" />
            </div>
          </div>
        </div>
        <div className="block-politics">
          <div className="block-politics_item">© Гросс маркет 2020</div>
          <div className="block-politics_item politic-modal">Политика обработки персональных данных</div>
        </div>
      </div>
    </footer>
  );
};
export default BaseFooter;
