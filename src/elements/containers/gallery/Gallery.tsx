import { FC, useState } from "react";
import "./Style.scss";

const Gallery: FC = () => {
  const [elementGalery, setElementGalery] = useState<number>(4);
  const cardsGallery = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
    { id: 8 },
    { id: 9 },
    { id: 10 },
    { id: 11 },
    { id: 12 },
    { id: 13 },
  ];

  const moreGalery = () => {
    if (cardsGallery.length - elementGalery >= 7) {
      setElementGalery(elementGalery + 8);
    } else {
      setElementGalery(elementGalery + cardsGallery.length - elementGalery);
    }
  };

  return (
    <section className="gallery-section">
      <h3 className="gallery-block__title">мы в инстаграме</h3>
      <div className="gallery-block">
        <div className="gallery-block-cards">
          {cardsGallery
            .filter((item, index) => index <= elementGalery)
            .map((item) => {
              return <div key={item.id} className="gallery-block__item"></div>;
            })}
        </div>
      </div>
      {elementGalery !== cardsGallery.length - 1 && (
        <button className="gallery-button" onClick={moreGalery}>
          Показать еще
        </button>
      )}
    </section>
  );
};
export default Gallery;
