import {
  Children,
  cloneElement,
  FC,
  ReactNode,
  useEffect,
  useState,
} from "react";
import ButtonArrow from "../buttonArrow/ButtonArrow";
import "./Style.scss";
import { v4 as uuidv4 } from "uuid";
interface ICarousel {
  children: ReactNode;
  classNameButton?: string;
  pageWidth: number;
  infinite?: boolean;
}

const Carousel: FC<ICarousel> = ({
  children,
  classNameButton,
  pageWidth,
  infinite,
}) => {
  const [pages, setPages] = useState<any>();
  const [offset, setOffset] = useState<number>(0);
  const [i, setI] = useState<number>(0);

  const onclickNext = () => {
    setOffset(offset - pageWidth);
    if (infinite) {
      setPages([...pages, { ...pages[i], key: uuidv4() }]);
      setI(i + 1);
    }
  };

  useEffect(() => {
    setPages(
      Children.map(children, (child) => {
        return cloneElement(child as React.ReactElement<any>, {
          style: {
            height: "100%",
            minWidth: `${pageWidth}vw`,
            maxWidth: `${pageWidth}vw`,
          },
        });
      })
    );
  }, [children, pageWidth]);

  return (
    <div className="main-container">
      <div className="window">
        <div
          style={{ transform: `translateX(${offset}vw)` }}
          className="all-pages-container"
        >
          {pages}
        </div>
      </div>
      <div className={classNameButton}>
        <ButtonArrow
          onClickNext={onclickNext}
          onClickPrev={() => setOffset(offset + pageWidth)}
          disabledPrev={offset === 0}
          disabledNext={offset === -(pageWidth * (pages?.length - 1))}
        />
      </div>
    </div>
  );
};
export default Carousel;
