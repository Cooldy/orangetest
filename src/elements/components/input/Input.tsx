/* eslint-disable no-useless-concat */
import { ChangeEvent, FC, useState } from "react";
import { handlePaste, handlePhoneFormat } from "../../../shared/maskPhoneInput";
import "./Style.scss";

type InputType = "text" | "date" | "radio" | "textarea" | "tel";

interface IInput {
  className?: string;
  label: string;
  required?: boolean;
  placeholder?: string;
  value?: string;
  type?: InputType;
  optionRadio?: { id: number; title: string; value: string }[];
  name?: string;
  textClass?: string;
  onChange?: (e: string) => void;
  valuePhone?: string;
}

const Input: FC<IInput> = ({
  placeholder = "введите значение",
  label,
  required = false,
  className,
  type = "text",
  optionRadio,
  name,
  textClass,
  onChange,
  valuePhone,
}) => {
  const [valueTextInput, setValueTextInput] = useState<string>("");
  const [valueDateInput, setValueDateInput] = useState<string>("");
  const [valueRadioInput, setValueRadioInput] = useState<string>("");

  const changeValue = (event: ChangeEvent<HTMLInputElement>) => {
    setValueRadioInput(event.target.value);
  };

  const renderInput = () => {
    switch (type) {
      case "text":
        return (
          <input
            className="input-content__value"
            type="text"
            placeholder={placeholder}
            value={valueTextInput}
            onChange={(event) => setValueTextInput(event.target.value)}
          />
        );
      case "date":
        return (
          <input
            className="input-content__value"
            type="date"
            value={valueDateInput}
            onChange={(event) => setValueDateInput(event.target.value)}
          />
        );
      case "radio":
        return (
          <div className="radio-content">
            {optionRadio?.map((item) => {
              return (
                <p key={item.id}>
                  <input
                    key={item.id}
                    className="radio-input"
                    type="radio"
                    name={name}
                    value={valueRadioInput}
                    onChange={(event) => changeValue(event)}
                  />
                  {item.title}
                </p>
              );
            })}
          </div>
        );
      case "textarea":
        return (
          <textarea
            className={`input-content__value ${textClass}`}
            value={valueDateInput}
            onChange={(event) => setValueDateInput(event.target.value)}
          />
        );
      case "tel":
        return (
          <input
            className="input-content__value"
            type="tel"
            maxLength={18}
            placeholder={placeholder}
            value={valuePhone}
            onChange={(e) => handlePhoneFormat(e, onChange)}
            onPaste={(e) => handlePaste(e, onChange)}
          />
        );
    }
  };

  return (
    <div className={`input-block ${className}`}>
      <div className="input-label">{required ? `${label} *` : label}</div>
      <div>
        <div
          className={type === "radio" ? "input-content_radio" : "input-content"}
        >
          {renderInput()}
        </div>
      </div>
    </div>
  );
};
export default Input;
