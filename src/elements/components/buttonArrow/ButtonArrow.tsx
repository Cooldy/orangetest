import { FC } from "react";
import { ReactComponent as Prev } from "../../../assets/image/icon/arrowLeft.svg";
import { ReactComponent as Next } from "../../../assets/image/icon/arrowRight.svg";
import "./Style.scss";

interface IButton {
  className?: string;
  disabledNext?: boolean;
  disabledPrev?: boolean;
  onClickNext?: () => void;
  onClickPrev?: () => void;
}

const ButtonArrow: FC<IButton> = ({
  className,
  onClickPrev,
  disabledPrev,
  onClickNext,
  disabledNext,
}) => {
  return (
    <div className={`buttons-arrows ${className}`}>
      <button
        className="button-arrows"
        onClick={onClickPrev}
        disabled={disabledPrev}
      >
        <Prev className={disabledPrev ? "arrow-disabled" : "arrow"} />
      </button>
      <button
        className="button-arrows next"
        onClick={onClickNext}
        disabled={disabledNext}
      >
        <Next className={disabledNext ? "arrow-disabled" : "arrow"} />
      </button>
    </div>
  );
};
export default ButtonArrow;
