import { FC } from "react";
import InfoRadius from "../InfoRadius/InfoRadius";
import "./Style.scss";

interface ICard {
  className?: string;
  titleInfo: string;
  src: string;
  text: string;
}

const Card: FC<ICard> = ({ className, titleInfo, src, text }) => {
  return (
    <div className={`card ${className}`}>
      <div className="card-block-image">
        <InfoRadius className="card-info" title={titleInfo} />
        <img alt="galery" className="card-image" src={src}  />
        <p className="card-text">{text}</p>
      </div>
    </div>
  );
};
export default Card;
