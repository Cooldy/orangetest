import { FC } from "react";
import "./Style.scss";
interface IInfoRadius {
  title: string;
  className?: string;
}

const InfoRadius: FC<IInfoRadius> = ({ title, className }) => {
  return <div className={`info-radius ${className}`}>{title}</div>;
};
export default InfoRadius;
