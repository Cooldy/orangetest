import { FC } from "react";
import { IUser } from "../../../types/types";

interface IUserList {
  users: IUser[];
}

const UserList: FC<IUserList> = ({ users }) => {

  return (
    <div>
      {users.map((user) => (
        <div key={user.id}>{user.name}</div>
      ))}
    </div>
  );
};

export default UserList;
