import { FC, useState } from "react";
import { ReactComponent as Arrow } from "../../../assets/image/icon/arrowRight.svg";
import "./Style.scss";

interface ISelect {
  classSelect?: string;
  initValue?: string;
  label: string;
  required?: boolean;
  option: { id: number; title: string }[];
  isSearch?: boolean;
}

const Select: FC<ISelect> = ({
  initValue = "введите значение",
  classSelect = "",
  required = true,
  option,
  isSearch = false,
}) => {
  const [isActiveList, setIsActiveList] = useState<boolean>(false);
  const [valueSelect, setValueSelect] = useState<string>(initValue);

  const changeValue = (title: string) => {
    setValueSelect(title);
    setIsActiveList(false);
  };

  return (
    <div className={`select__container ${classSelect}`}>
      <div
        className={isActiveList ? "select -active" : "select"}
        onClick={() => setIsActiveList(!isActiveList)}
      >
        <input
          className={
            valueSelect === initValue
              ? "select__input -initial"
              : "select__input"
          }
          disabled={!isSearch}
          value={valueSelect}
          onChange={(e) => setValueSelect(e.target.value)}
        />
        <Arrow
          className={
            isActiveList ? "select__input-arrow -active" : "select__input-arrow"
          }
          onClick={() => setIsActiveList(!isActiveList)}
        />
      </div>
      <ul
        className={isActiveList ? "select__option -active" : "select__option"}
      >
        {option.map((item) => {
          return (
            <li
              className="select__option__item"
              key={item.id}
              onClick={() => changeValue(item.title)}
            >
              {item.title}
            </li>
          );
        })}
      </ul>
    </div>
  );
};
export default Select;
