import { FC, ReactNode } from "react";
import "./Style.scss";

interface IModal {
  active: boolean;
  setActive: (active: boolean) => void;
  children?: ReactNode;
}

const Modal: FC<IModal> = ({ active, setActive, children }) => {
  return (
    <div
      className={active ? "modal active" : "modal"}
      onClick={() => setActive(false)}
    >
      <div
        className={active ? "modal_content active" : "modal_content"}
        onClick={(e) => e.stopPropagation()}
      >
        <div className="moda_cross">
          <span onClick={() => setActive(false)}>&#10006;</span>
        </div>
        {children}
      </div>
    </div>
  );
};
export default Modal;
