import { FC } from "react";
import CardSlider from "../../containers/cardSlider/CardSlider";
import Gallery from "../../containers/gallery/Gallery";
import MainSlider from "../../containers/MainSlider/MainSlider";

const MainPage: FC = () => {
  return (
    <div>
      <MainSlider />
      <CardSlider />
      <Gallery />
    </div>
  );
};
export default MainPage;
