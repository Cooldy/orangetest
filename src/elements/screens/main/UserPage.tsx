import { FC, useEffect } from "react";
import { useAction } from "../../../hook/useAction";
import { useTypedSelector } from "../../../hook/useTypedSelector";
import UserList from "../../components/user/UserList";


const UserPage:FC = () => {
  const { users, error, loading } = useTypedSelector((state) => state.user);
  const { fetchUsers } = useAction();

  useEffect(() => {
    fetchUsers();
  }, []);

  return (
    <div className="App">
     <UserList users={users}/>
    </div>
  );
};

export default UserPage;
