import { FC, ReactNode } from "react";
import BaseFooter from "../../containers/footer/BaseFooter";
import BaseHeader from "../../containers/header/BaseHeader";

interface ILayout {
  children: ReactNode;
}

const BaseLayout: FC<ILayout> = ({ children }) => {
  return (
    <main className="base-layout">
      <BaseHeader />
      {children}
      <BaseFooter />
    </main>
  );
};

export default BaseLayout;
